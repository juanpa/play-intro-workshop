package controllers

import play.api.mvc._

class Practices0 extends Controller {

    def list() = Action {
        Ok("[]").as(JSON)
    }

    def detail(id: Long) = Action {
        Ok(s"""{ "id": $id }""").as(JSON)
    }
}


/**
 * Python:
 *
 * class Practices0(Controller):
 *
 *     @ Action
 *     def list(self):
 *         return Ok("[]").as(JSON)
 *
 *    ## or
 *
 *    def list(self):
 *        return Action(lambda: Ok("[]").as(JSON))
 */
