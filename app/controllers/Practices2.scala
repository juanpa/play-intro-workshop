package controllers

import models.{ContactInfo, Practice}
import play.api.libs.json.Json
import play.api.mvc._
import services.{PracticeDAO, PracticesService}
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Practices2 extends Controller {

    def dao = new PracticeDAO

    def list(expand: Boolean) = Action.async {

        dao.all() map { row ⇒ Ok(Json.toJson(row)) }

    }

    def detail(id: Long) = Action {

        PracticesService.get(id) match {
            case Some(practice) ⇒ Ok(Json.toJson(practice))
            case None ⇒ NotFound
        }
    }

    def detailContact(id: Long) = Action {

        PracticesService.get(id) match {
            case Some(practice) ⇒ Ok(Json.toJson(practice.contactInfo))
            case None ⇒ NotFound
        }
    }

    // implicit objects required to transform the models into json

    implicit val contactInfoFormat = Json.format[ContactInfo]

    // which is equivalent to:

    //  implicit val contactInfoWrites = Json.writes[ContactInfo]
    //  implicit val contactInfoReads = Json.reads[ContactInfo]


    implicit val practiceWrites = Json.writes[Practice]

    // ^^ can also be put into the companion objects



}
