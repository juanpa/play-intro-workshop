package controllers

import models.ContactInfo
import play.api.libs.json.Json
import play.api.mvc.{Action, Controller}
import services.PracticeDAO
import play.api.libs.concurrent.Execution.Implicits.defaultContext

import scala.concurrent.Future

class ContactInfos extends Controller {

    def dao = new PracticeDAO

    def list() = Action.async {
        val ciF: Future[List[ContactInfo]] = dao.contactInfos()
        ciF.map(infoes ⇒ Ok(Json.toJson(infoes)))
    }

    implicit val contactInfoFormat = Json.format[ContactInfo]

}
