package controllers

import play.api._
import play.api.mvc._

class Application extends Controller {

    def endpoint() = Action {
        Ok("hello world!")
    }

    def hello(name: String) = Action {
        Ok("Hello " + name)
    }

    def redirect() = Action {
//        Redirect("/endpoint")
        Redirect(routes.Application.endpoint)
    }

}
