package controllers

import models.{ContactInfo, Practice}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.libs.ws.{WSResponse, WSClient}
import play.api.mvc._
import services.{PracticeDAO, PracticesService}
import javax.inject.Inject
import scala.concurrent.Future

class Practices3 @Inject() (ws: WSClient) extends Controller {

    val base = "http://127.0.0.1:8001/v2"

    def schema() = Action.async {
        val future: Future[WSResponse] = ws.url(base).get()
        future.map { response ⇒
            Ok(response.json)
        }
    }


    // implicit objects required to transform the models into json

    implicit val contactInfoFormat = Json.format[ContactInfo]

    // which is equivalent to:

    //  implicit val contactInfoWrites = Json.writes[ContactInfo]
    //  implicit val contactInfoReads = Json.reads[ContactInfo]


    implicit val practiceWrites = Json.writes[Practice]

    // ^^ can also be put into the companion objects



}
