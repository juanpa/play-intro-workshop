package controllers

import models.{Practice, ContactInfo}
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.mvc._
import services.PracticesService

class Practices1 extends Controller {

    def listIds() = Action {
        val ids: List[Long] = PracticesService.practices.map(practice ⇒ practice.id)

        // toJson works out of the box for in built types (like list longs)
        Ok(Json.toJson(ids))
    }

    def list() = Action {
        // for other objects we need to create the implicits below
        Ok(Json.toJson(PracticesService.practices))
    }

    def detail(id: Long) = Action {

        PracticesService.get(id) match {
            case Some(practice) ⇒ Ok(Json.toJson(practice))
            case None ⇒ NotFound
        }
    }

    def detailContact(id: Long) = Action {

        PracticesService.get(id) match {
            case Some(practice) ⇒ Ok(Json.toJson(practice.contactInfo))
            case None ⇒ NotFound
        }
    }

    // implicit objects required to transform the models into json

    implicit val contactInfoFormat = Json.format[ContactInfo]

    // which is equivalent to:

    //  implicit val contactInfoWrites = Json.writes[ContactInfo]
    //  implicit val contactInfoReads = Json.reads[ContactInfo]


    implicit val practiceWrites = Json.writes[Practice]

    // ^^ can also be put into the companion objects



    /**
     * example:
     *  curl -X PUT -H "Content-Type: application/json" -d '{"email":"asdf","phone":"1","address":"","country":"","state":"","city":"","zipCode":""}'  localhost:9000/v1/practices/1/contact
     */

    def updateContact(id: Long) = Action(BodyParsers.parse.json) { request ⇒

        val validateInfo = request.body.validate[ContactInfo]

        validateInfo.fold(
            errors ⇒ BadRequest,
            contact ⇒ {
                PracticesService.get(id) match {
                    case Some(practice) ⇒
                        val copy = practice.copy(contactInfo = contact)
                        PracticesService.save(copy)
                        Ok(Json.toJson(copy))
                    case None ⇒ NotFound
                }
            }
        )

    }
}
