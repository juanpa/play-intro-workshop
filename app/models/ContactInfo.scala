package models

import play.api.libs.json.Json

/**
 * A case class is a normal class that can be constructed without `new`
 *
 * i.e. val c = ContactInfo(...)
 *
 * it also can be easily pattern matched
 *
 */
case class ContactInfo (id: Int, email: String, phone: String = "", address: String = "" )



/**
 * Python equivalent:
 *
 * class ContactInfo(object):
 *      def __init__(self, email, phone = "", address = "", ...):
 *          self.email = email
 *          self.phone = phone
 *          self.address = address
 *          ...
 */

