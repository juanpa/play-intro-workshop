package models


case class Practice
(
    id: Long = -1,
    name: String,
    contactInfo: ContactInfo = ContactInfo(-1, "")
)
/**
 * Python:
 *
 * class Practice(object):
 *   def __init__(self, name, contactInfo, id = -1):
 *        self.name = name
 *        self.contactInfo = contactInfo or ContactInfo("")
 */

