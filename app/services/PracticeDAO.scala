package services

import play.api.libs.json.Json

import scala.concurrent.Future

import models.{ContactInfo, Practice}
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

case class PracticeShallow(id: Int, name: String, contactInfoId: Option[Int])

object PracticeShallow {
    implicit val format = Json.format[PracticeShallow]
}

class PracticeDAO extends HasDatabaseConfig[JdbcProfile] {

    val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)

    import driver.api._

    private val Practices = TableQuery[PracticesTable]
    private val ContactInfos = TableQuery[ContactInfoTable]

    def contactInfos(): Future[List[ContactInfo]] = db.run(ContactInfos.result).map(_.toList)

    def all() = db.run(Practices.result).map(_.toList)

//    def allWithContactInfo()  = {
//        val psF = db.run(Practices.result).map(_.toList)
//        val ids = psF.map(ps ⇒ ps.map(_.id))
//
//    }

    class ContactInfoTable(tag: Tag) extends Table[ContactInfo](tag, "locus_lib_reqrepo_contactinfo") {

        def id = column[Int]("id", O.PrimaryKey)
        def email = column[String]("email")
        def phone = column[String]("phone")
        def address = column[String]("address_1")

        def * = (id, email, phone, address) <> (ContactInfo.tupled, ContactInfo.unapply _)
    }

    class PracticesTable(tag: Tag) extends Table[PracticeShallow](tag, "locus_lib_reqrepo_practice") {

        def id = column[Int]("id", O.PrimaryKey)
        def name = column[String]("name")
        def contactInfoId = column[Option[Int]]("contact_info_id")

        def * = (id, name, contactInfoId) <> ((PracticeShallow.apply _).tupled, PracticeShallow.unapply _)

        def contactInfo = foreignKey("contact_info_id", contactInfoId, ContactInfos)(_.id)

    }
}
