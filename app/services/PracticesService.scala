package services

import models.{ContactInfo, Practice}

// a singleton object
// similar to a static class in other languages

object PracticesService {

    // it has to be a `var` so we can mutate it
    var maxId = 2

    var practices = {
        List(
            Practice(1, "Invitae Test Practice", ContactInfo(1, "invitae@mail.com", "415-534-9127")),
            Practice(2, "Another Practice", ContactInfo(2, "sample@mail.com", "415-534-9127"))
        )
    }

    def get(id: Long): Option[Practice] = {
        practices.find(practice ⇒ practice.id == id)
    }

    /**
     * Create or update
     * @param practice
     */
    def save(practice: Practice) = {
        val lengthBefore = practices.length
        practices = practices.filterNot(p ⇒ p.id == practice.id)

        if(lengthBefore == practices.length) {
            // practice was not in practices, append it with a new id
            maxId += 1
            practices = practices ++ List(practice.copy(id = maxId))
        } else {
            // practice with the same id existed in practices, just add the new
            // object, in effect updating
            practices = practices ++ List(practice)
        }
    }

    def filterByEmail(email: String) = {
        practices.filter(practice ⇒ practice.contactInfo.email == email)
    }
}


/**
 * Python:
 *
 * class PracticesService(object):
 *
 *     practices = [
 *         Practice("Invitae Test Practice", ContactInfo("invitae@mail.com", "415-534-9127")),
 *         Practice(...)
 *     ]
 *
 *     @ staticmethod
 *     def save(practice):
 *         PracticesService.practices = PracticesService.practices + [practice]
 */
